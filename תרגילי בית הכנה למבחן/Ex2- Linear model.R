#EX2 - Linear model
bike <- read.csv("train.csv", header=T)
str(bike)

#split the data by date
bike$datetime <- as.character(bike$datetime) #Convert date to character
bike$date <- sapply(strsplit(bike$datetime,' '), "[", 1) #Choose only date
bike$date <- as.Date(bike$date) #Convert date to date type
bike$time <- sapply(strsplit(bike$datetime,' '), "[", 2) #Choose only time
bike$hour <-  sapply(strsplit(bike$time,':'), "[", 1) #Choose only hour
bike$hour <- as.numeric(bike$hour) #Convert hour to numeric
bike$time <- NULL #cancel column time
bike$datetime <- NULL #cancel column dateTime

#-------------------------------------------
#Data preperation
#Check missing values
any(is.na(bike))
#Take only numeic values
col.numeric <- sapply(bike, is.numeric)
bike.numeric <- bike[,col.numeric]

#Correlation
correlation <- cor(bike.numeric)
library(corrplot)
print(corrplot(correlation, method = 'color'))
#Between count (target) to registerd, casual, temp, humidity, hour have correlation
#the two first have high correlation and the other have week

#Split to taining and test set
splitDate <- as.Date('2012-05-01')
dateFilter <- bike$date <= splitDate

train <- bike[dateFilter,]
test <- bike[!dateFilter,]

#------------------------------------------
#Model
model <- lm(count ~ . -atemp -season -holiday -workingday -weather -windspeed, train)
print(summary(model))
#we see that registerd, casual, temp, humidity are influence on the count (target)

#Predict
predicted <- predict(model,test)
results <- cbind(predicted, test$count)
colnames(results) <- c('Predicted', 'Actual')
results <- as.data.frame(results)

#Cutting negative values 
to_zero <- function(x){
  if(x<0){
    return (0)
  } else {
    return (x)
  }
}
results$Predicted <- sapply(results$Predicted,to_zero)

min(results$Predicted) #Check there no negative values

MSE <- mean((results$Actual-results$Predicted)^2)
RMSE <- MSE^0.5

#-----------------------------------------
#OverFitting
predicted.train <- predict(model,train)
results.train <- cbind(predicted.train, train$count)
colnames(results.train) <- c('Predicted', 'Actual')
results.train <- as.data.frame(results.train)

results.train$Predicted <- sapply(results.train$Predicted,to_zero)

min(results.train$Predicted) #Check there no negative values

MSE.train <- mean((results.train$Actual-results.train$Predicted)^2)
RMSE.tain <- MSE.train^0.5

overFitting <- ((RMSE-RMSE.tain)/RMSE)*100
##there is a overfittinh (RMSE in test is smaller than in train)
#When the model more accurate on training set, we see that exist overFitting