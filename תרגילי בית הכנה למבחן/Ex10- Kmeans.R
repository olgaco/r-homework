#EX10 - kmeans

redWines  <- read.csv('whiteWines.csv', sep = ';')
whiteWines  <- read.csv('redWines.csv', sep = ';')
str(redWines)
str(whiteWines)

redWines$type <- 'white'
whiteWines$type <- 'red'

Wines <- rbind(redWines, whiteWines)
nrow(redWines)+nrow(whiteWines)
nrow(Wines)

Wines$type <- as.factor(Wines$type)
str(Wines)

#Show a histogram of residual.sugar fill it with the red and white labels. Whats the conclusions? 
library(ggplot2)
ggplot(Wines, aes(x=residual.sugar, fill=type)) + geom_histogram()
#Conclusion - we dont see big diffrent between them

#Make a similar analysis for the alcohol level 
ggplot(Wines, aes(x=alcohol, fill=type)) + geom_histogram()
#Conclusion - we dont see big diffrent between them

#Make a few scatter plots to seek for a separating factor 
ggplot(Wines, aes(x= quality,y= alcohol, color=type)) + geom_point()
#No relation
ggplot(Wines, aes(residual.sugar,alcohol, color=type)) + geom_point()+xlim(0, 25)
#No relation

set.seed(101)

#Run the model
Cluster <- kmeans(Wines[, 1:12], 2, nstart = 20)
Cluster$cluster #see the groups
table(Cluster$cluster, Wines$type)

#   red white
#1 1514  1294  #1=red
#2   85  3604  #2=white

#Graph
library(cluster)
clusplot(Wines, Cluster$cluster, color=T, shade = T, labels = 0, lines = 0)