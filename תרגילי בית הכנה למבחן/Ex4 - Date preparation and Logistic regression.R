library(ggplot2)
install.packages("dplyr")
install.packages("Amelia")
library(dplyr)
library(Amelia)

titanic<- read.csv('titanic.csv')
str(titanic)

#checking missing values
missmap(titanic, main= "Missing data", col= c('yellow','black'))
#emputation
ageMissing <- is.na(titanic$Age)

meanage<- mean(titanic$Age[!ageMissing])

imputeAge<- function(age){
  if(is.na(age)){
    return(meanage)
  }
  else{
    return(age)
  }
}
#replace NA with mean value of age
titanic$Age <- sapply(titanic$Age,imputeAge)

ggplot(titanic,aes(Survived)) + geom_bar()

ggplot(titanic, aes(Age, fill= factor(Survived))) + geom_histogram(binwidth = 20)

ggplot(titanic, aes(Fare, fill= factor(Survived))) + geom_histogram(binwidth = 50)

#clean irrelavent columns
titanic.clean<-select(titanic,-PassengerId,-Name,-Ticket,-Cabin)
str(titanic.clean)

titanic.clean$Survived <- factor(titanic.clean$Survived)
titanic.clean$Pclass <- factor(titanic.clean$Pclass)

str(titanic.clean)

#split to training and test sets
titanic.clean.train <- sample_frac(titanic.clean, 0.7)
#save all row numbers that are in train test
sid<- as.numeric(rownames(titanic.clean.train))

titanic.clean.test<- titanic.clean[-sid,]

#logistic model


log.model <- glm(Survived~.,family = binomial(link = 'logit'),titanic.clean.train )
summary(log.model)

#all varaibels that have * - was found as influence on the probebility. 

#check the probebility on the test set
predicred.probabilities <- predict(log.model, titanic.clean.test, type= 'response')
str(titanic.clean.test)

#under 0.5- Not survived,
predicted.values <- ifelse(predicred.probabilities>0.5,1,0)

#In which part the prediction is wrong (compare predicted to test)
misClassError<- mean(predicted.values != titanic.clean.test$Survived)
#20%- we was wrong, 80% was right

